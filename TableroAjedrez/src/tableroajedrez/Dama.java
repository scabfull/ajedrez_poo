package tableroajedrez;

public class Dama extends Pieza {

    private final char simbolo = 'D';
    char letra = simbolo;

    public Dama(Tablero tablero, char color, char coordY, int coordX) {
        this.color = color;
        this.casilla = tablero.casillas[coordX-1][tablero.coordYNumber(coordY)];
        tablero.casillas[coordX-1][tablero.coordYNumber(coordY)].setPieza(letra);
        tablero.casillas[coordX-1][tablero.coordYNumber(coordY)].setColor(this.color);
    }

    public char getSimbolo() {
        return simbolo;
    }

}
