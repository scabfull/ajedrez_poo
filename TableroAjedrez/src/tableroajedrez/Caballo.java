package tableroajedrez;

public class Caballo extends Pieza {

    private final char simbolo = 'C';
    char letra = simbolo;

    public Caballo(Tablero tablero, char color, char coordY, int coordX) {
        this.color = color;
        this.casilla = tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)];
        tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)].setPieza(letra);
        tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)].setColor(this.color);
    }

    public boolean mover(Tablero t, Casilla c) {
        int newX = c.getCoordenadaX();
        int oldX = this.casilla.getCoordenadaX();
        int newY = t.coordYNumber(c.getCoordenadaY());
        int oldY = t.coordYNumber(this.casilla.getCoordenadaY());
        if (((newX == oldX + 2 && (newY == oldY + 1 || newY == oldY - 1))
                || (newX == oldX - 2 && (newY == oldY + 1 || newY == oldY - 1))
                || (newY == oldY + 2 && (newX == oldX + 1 || newX == oldX - 1))
                || (newY == oldY - 2 && (newX == oldX + 1 || newX == oldX - 1))) && t.casillas[newX][newY].getPieza() != 'R' && t.casillas[newX][newY].getColor() != this.getColor()) {
            t.casillas[oldX][oldY].setPieza('-');
            t.casillas[oldX][oldY].setColor('-');
            t.casillas[newX][newY].setPieza(this.letra);
            t.casillas[newX][newY].setColor(this.color);
            this.setCasilla(c);
            System.out.println("Pieza movida");
            return true;
        } else {
            System.out.println("No ha sido posible mover la pieza");
            return false;
        }
    }

    public char getSimbolo() {
        return simbolo;
    }

}
