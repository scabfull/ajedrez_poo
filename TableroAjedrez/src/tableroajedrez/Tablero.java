package tableroajedrez;

public class Tablero {

    public Casilla[][] casillas = new Casilla[8][8];

    public Tablero() {

        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                casillas[i][j] = new Casilla(i, coordYLetter(j));
            }
        }
    }

    public void pintarTablero() {
        System.out.println();
        for (int i = 7; i >= 0; i--) {
            for (int j = 0; j < 8; j++) {
                System.out.print("[" + casillas[i][j].getPieza() + casillas[i][j].getColor() + "] ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int coordYNumber(char y) {
        switch (y) {
            case 'a':
                return 0;
            case 'b':
                return 1;
            case 'c':
                return 2;
            case 'd':
                return 3;
            case 'e':
                return 4;
            case 'f':
                return 5;
            case 'g':
                return 6;
            case 'h':
                return 7;
            default:
                System.out.println("Coordenada Y incorrecta: " + y + ". Debe ser entre 'a' y 'h'");
                return -1;
        }
    } //Te dice qué número de coordenada es la letra que le pases

    public char coordYLetter(int y) {
        switch (y) {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
            default:
                System.out.println("Coordenada Y incorrecta: " + y + ". Debe ser entre 0 y 7");
                return 'z';
        }
    } //Te dice qué letra le corresponde a ese número de coordenada

}
