package tableroajedrez;

public class Casilla {

    private int coordenadaX;
    private char coordenadaY;
    private boolean libre;
    private char pieza;
    private char color;

    public Casilla(int x, char y) {
        this.setCoordenadaX(x);
        this.setCoordenadaY(y);
        this.setLibre(true);
        this.setPieza('-');
        this.setColor('-');
    }

    public int getCoordenadaX() {
        return coordenadaX;
    }

    public void setCoordenadaX(int coordenadaX) {
        if (coordenadaX < 0 || coordenadaX > 7) {
            System.out.println("Coordenada X incorrecta: " + coordenadaX + ". Debe ser entre 0 y 7");
        } else {
            this.coordenadaX = coordenadaX;
        }
    }

    public char getCoordenadaY() {
        return coordenadaY;
    }

    public void setCoordenadaY(char coordenadaY) {
        this.coordenadaY = coordenadaY;
    }

    public boolean isLibre() {
        return libre;
    }

    public void setLibre(boolean libre) {
        this.libre = libre;
    }

    public char getPieza() {
        return pieza;
    }

    public void setPieza(char pieza) {
        this.pieza = pieza;
    }

    public char getColor() {
        return color;
    }

    public void setColor(char color) {
        this.color = color;
    }

}
