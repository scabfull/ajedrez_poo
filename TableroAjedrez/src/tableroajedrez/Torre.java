package tableroajedrez;

public class Torre extends Pieza {

    private final char simbolo = 'T';
    char letra = simbolo;

    public Torre(Tablero tablero, char color, char coordY, int coordX) {
        this.color = color;
        this.casilla = tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)];
        tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)].setPieza(letra);
        tablero.casillas[coordX - 1][tablero.coordYNumber(coordY)].setColor(this.color);
    }

    public boolean mover(Tablero t, Casilla c) {
        int newX = c.getCoordenadaX();
        int oldX = this.casilla.getCoordenadaX();
        int newY = t.coordYNumber(c.getCoordenadaY());
        int oldY = t.coordYNumber(this.casilla.getCoordenadaY());
        if ((oldX == newX) && (oldY == newY)) {//Caso en el que x e y son iguales
            System.out.println("Movimiento incorrecto: la pieza no puede quedarse en la misma casilla");
            return false;
        } else if ((oldX != newX) && (oldY != newY)) {//Caso en el que tanto x como y son distintos (movimiento diagonal)
            System.out.println("Movimiento incorrecto: la torre no puede moverse en diagonal");
            return false;
        } else if (oldY == newY) {//Caso en el que se mueve en X
            if (newX > oldX) {//Caso en el que se mueve hacia arriba
                boolean caminoLibre = true;
                boolean caminoYDestinoLibre = true;
                for (int i = oldX + 1; i < newX; i++) {
                    if (!(t.casillas[i][oldY].getPieza() == '-')) {
                        caminoLibre = false;
                    }
                }
                for (int i = oldX + 1; i <= newX; i++) {
                    if (!(t.casillas[i][oldY].getPieza() == '-')) {
                        caminoYDestinoLibre = false;
                    }
                }
                if (caminoYDestinoLibre || (t.casillas[newX][newY].getColor() != this.getColor() && t.casillas[newX][newY].getPieza() != 'R' && caminoLibre)) {
                    t.casillas[oldX][oldY].setPieza('-');
                    t.casillas[oldX][oldY].setColor('-');
                    t.casillas[newX][newY].setPieza(this.letra);
                    t.casillas[newX][newY].setColor(this.color);
                    this.setCasilla(c);
                    System.out.println("Pieza movida");
                    return true;
                } else {
                    System.out.println("No se ha podido mover la pieza: camino obstruido");
                    return false;
                }
            } else {//Caso en el que se mueve hacia abajo
                boolean caminoLibre = true;
                boolean caminoYDestinoLibre = true;
                for (int i = oldX - 1; i > newX; i--) {
                    if (!(t.casillas[i][oldY].getPieza() == '-')) {
                        caminoLibre = false;
                    }
                }
                for (int i = oldX - 1; i >= newX; i--) {
                    if (!(t.casillas[i][oldY].getPieza() == '-')) {
                        caminoYDestinoLibre = false;
                    }
                }
                if (caminoYDestinoLibre || (t.casillas[newX][newY].getColor() != this.getColor() && t.casillas[newX][newY].getPieza() != 'R' && caminoLibre)) {
                    t.casillas[oldX][oldY].setPieza('-');
                    t.casillas[oldX][oldY].setColor('-');
                    t.casillas[newX][newY].setPieza(this.letra);
                    t.casillas[newX][newY].setColor(this.color);
                    this.setCasilla(c);
                    System.out.println("Pieza movida");
                    return true;
                } else {
                    System.out.println("No se ha podido mover la pieza: camino obstruido");
                    return false;
                }
            }
        } else if (oldX == newX) {//Caso en el que se mueve en el eje Y
            if (newY > oldY) {//Caso en el que se mueve hacia la derecha
                boolean caminoLibre = true;
                boolean caminoYDestinoLibre = true;
                for (int i = oldY + 1; i < newY; i++) {
                    if (!(t.casillas[oldX][i].getPieza() == '-')) {
                        caminoLibre = false;
                    }
                }
                for (int i = oldY + 1; i <= newY; i++) {
                    if (!(t.casillas[oldX][i].getPieza() == '-')) {
                        caminoYDestinoLibre = false;
                    }
                }
                if (caminoYDestinoLibre || (t.casillas[newX][newY].getColor() != this.getColor() && t.casillas[newX][newY].getPieza() != 'R' && caminoLibre)) {
                    t.casillas[oldX][oldY].setPieza('-');
                    t.casillas[oldX][oldY].setColor('-');
                    t.casillas[newX][newY].setPieza(this.letra);
                    t.casillas[newX][newY].setColor(this.color);
                    this.setCasilla(c);
                    System.out.println("Pieza movida");
                    return true;
                } else {
                    System.out.println("No se ha podido mover la pieza: camino obstruido");
                    return false;
                }
            } else {//Caso en el que se mueve hacia la izquierda
                boolean caminoLibre = true;
                boolean caminoYDestinoLibre = true;
                for (int i = oldY - 1; i > newY; i--) {
                    if (!(t.casillas[oldX][i].getPieza() == '-')) {
                        caminoLibre = false;
                    }
                }
                for (int i = oldY - 1; i >= newY; i--) {
                    if (!(t.casillas[oldX][i].getPieza() == '-')) {
                        caminoYDestinoLibre = false;
                    }
                }
                if (caminoYDestinoLibre || (t.casillas[newX][newY].getColor() != this.getColor() && t.casillas[newX][newY].getPieza() != 'R' && caminoLibre)) {
                    t.casillas[oldX][oldY].setPieza('-');
                    t.casillas[oldX][oldY].setColor('-');
                    t.casillas[newX][newY].setPieza(this.letra);
                    t.casillas[newX][newY].setColor(this.color);
                    this.setCasilla(c);
                    System.out.println("Pieza movida");
                    return true;
                } else {
                    System.out.println("No se ha podido mover la pieza: camino obstruido");
                    return false;
                }
            }
        }
        return false;
    }

    public char getSimbolo() {
        return simbolo;
    }

}
