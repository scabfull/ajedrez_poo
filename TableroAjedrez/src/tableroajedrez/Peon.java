package tableroajedrez;

public class Peon extends Pieza {

    private final char simbolo = 'P';
    char letra = simbolo;

    public Peon(Tablero tablero, char color, char coordY, int coordX) {
        this.color = color;
        this.casilla = tablero.casillas[coordX-1][tablero.coordYNumber(coordY)];
        tablero.casillas[coordX-1][tablero.coordYNumber(coordY)].setPieza(letra);
        tablero.casillas[coordX-1][tablero.coordYNumber(coordY)].setColor(this.color);
    }

    public char getSimbolo() {
        return simbolo;
    }

}
