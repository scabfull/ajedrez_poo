package tableroajedrez;

public class TableroAjedrez {

    public static void main(String[] args) {
        Tablero test = new Tablero();
        //tableroInicial(test);
        Torre torre1 = new Torre(test, 'B', 'b', 2);
        Dama dama1 = new Dama(test, 'N', 'b', 4);
        Caballo caballo1 = new Caballo(test, 'N', 'b', 3);
        Peon peon1 = new Peon(test, 'N', 'c', 5);
        test.pintarTablero();
        caballo1.mover(test, test.casillas[4][2]);
        test.pintarTablero();
        torre1.mover(test, test.casillas[2][1]);
        test.pintarTablero();

    }

    public static void tableroInicial(Tablero tablero) {
        //Peones
        Peon peonblanco1 = new Peon(tablero, 'B', 'a', 2);
        Peon peonblanco2 = new Peon(tablero, 'B', 'b', 2);
        Peon peonblanco3 = new Peon(tablero, 'B', 'c', 2);
        Peon peonblanco4 = new Peon(tablero, 'B', 'd', 2);
        Peon peonblanco5 = new Peon(tablero, 'B', 'e', 2);
        Peon peonblanco6 = new Peon(tablero, 'B', 'f', 2);
        Peon peonblanco7 = new Peon(tablero, 'B', 'g', 2);
        Peon peonblanco8 = new Peon(tablero, 'B', 'h', 2);
        Peon peonNegro1 = new Peon(tablero, 'N', 'a', 7);
        Peon peonNegro2 = new Peon(tablero, 'N', 'b', 7);
        Peon peonNegro3 = new Peon(tablero, 'N', 'c', 7);
        Peon peonNegro4 = new Peon(tablero, 'N', 'd', 7);
        Peon peonNegro5 = new Peon(tablero, 'N', 'e', 7);
        Peon peonNegro6 = new Peon(tablero, 'N', 'f', 7);
        Peon peonNegro7 = new Peon(tablero, 'N', 'g', 7);
        Peon peonNegro8 = new Peon(tablero, 'N', 'h', 7);
        //Torres
        Torre torreBlanca1 = new Torre(tablero, 'B', 'a', 1);
        Torre torreBlanca2 = new Torre(tablero, 'B', 'h', 1);
        Torre torreNegra1 = new Torre(tablero, 'N', 'a', 8);
        Torre torreNegra2 = new Torre(tablero, 'N', 'h', 8);
        //Caballos
        Caballo caballoBlanco1 = new Caballo(tablero, 'B', 'b', 1);
        Caballo caballoBlanco2 = new Caballo(tablero, 'B', 'g', 1);
        Caballo caballoNegro1 = new Caballo(tablero, 'N', 'b', 8);
        Caballo caballoNegro2 = new Caballo(tablero, 'N', 'g', 8);
        //Alfiles
        Alfil alfilBlanco1 = new Alfil(tablero, 'B', 'c', 1);
        Alfil alfilBlanco2 = new Alfil(tablero, 'B', 'f', 1);
        Alfil alfilNegro1 = new Alfil(tablero, 'N', 'c', 8);
        Alfil alfilNegro2 = new Alfil(tablero, 'N', 'f', 8);
        //Damas
        Dama damaBlanca = new Dama(tablero, 'B', 'd', 1);
        Dama damaNegra = new Dama(tablero, 'N', 'd', 8);
        //Reyes
        Rey reyBlanco = new Rey(tablero, 'B', 'e', 1);
        Rey reyNegro = new Rey(tablero, 'N', 'e', 8);
    }

}
